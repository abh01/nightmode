#!/bin/bash

# TintIt v1.0

#This program can tint your screen upto the point, where you cant see anything in your disply
#Good for eyes and surely increases the productivity and eye dryness

#Author : Aravinda Bhat
#License : Dont worry, use it for whatever you want...
#Special thanks to random guy on the internet for helping me with ~AWK text manipulation~ 


tput setaf 2;echo "

          A          BBBBBBBB    H      H
         A A         B       B   H      H
        A   A        B        B  H      H
       A     A       B       B   H      H
      A       A      B   BBB     HHHHHHHH
     A  AAAAA  A     B       B   H      H
    A           A    B        B  H      H
   A             A   B       B   H      H I MAKE
  A               A  BBBBBBBB    H      H SIMPLE PROGRAMS COMPLEX 
                                         
 "


#XrandR is a CLI for RandR extension, which displays the display resolution information
displayInfo=$(xrandr -q | grep "connected" | head -n1 | awk '{print $1;}' )

tput setaf 3; echo "Enter the desigred level of tint (Between 0 and 1)"
tput setaf 3; echo "0 for high tint and 1 for no tint"
tput setaf 1; echo "Enter 0 at your own risk, I wont recomend doing it :("
read tintLevel

#check man page for all the details about the arguments and options passed in
xrandr --output $displayInfo --brightness $tintLevel

#Linux desktop notification api
notify-send "Tint Level set to $tintLevel"


#Version 1.0
#Version 2 changelog > Gamma Color Changing for more customization

