# NightMode
NightMode for linux is a simple shell script to reduce the screen brightness at night time

>It uses xrandr , CLI interface for randR

To run the script, just paste this to your terminal 
`$ sh NightMode.sh`

You can get the best out of this script, when you put this in `cron`. To know more about it
[Click here](http://man7.org/linux/man-pages/man8/cron.8.html) to visit man page
or , `$ man cron`


**Free Script, Hell Yeah!**
